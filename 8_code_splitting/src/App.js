import React, { lazy, Suspense } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import Home from "./Home";
import Dashboard from "./Dashboard";

// Для SSR придется использовать loadable-components библиотеку
// Suspend и lazy на сервере не работают.
// Основной механизм здесь - это React.lazy и import()
const About = lazy(() => import("./About"));

export default function BasicExample() {
  return (
    <Router>
      <div>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/about">About</Link>
          </li>
          <li>
            <Link to="/dashboard">Dashboard</Link>
          </li>
        </ul>

        <hr />

        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/about">
            {/* Так же можно обернуть это все в ErrorBoundary */}
            <Suspense fallback={<div>Загрузка...</div>}>
              <About />
            </Suspense>
          </Route>
          <Route path="/dashboard">
            <Dashboard />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}
